let env = require('dotenv')
//initialize .env file
env.config();

let http = require('http');
let express = require("express");
let RED = require("node-red");
let cors = require("cors");
let nodeRedSettings = require('./node-red-settings.ts');

// Create an Express app
let app = express();

// Add a simple route for static content served from 'public'
app.use("/", express.static("public"));
app.use(cors());

// Create a server
let server = http.createServer(app);

// Initialise the runtime with a server and settings
RED.init(server, nodeRedSettings);

// Serve the editor UI from /red
app.use(nodeRedSettings.httpAdminRoot, RED.httpAdmin);

// Serve the http nodes UI from /api
app.use(nodeRedSettings.httpNodeRoot, RED.httpNode);

server.listen(process.env.APP_PORT_NO);

// Start the runtime
RED.start();

const path = require('path');
const app1 = express();
app1.use(express.static(path.join(__dirname, 'guide')));
app1.get('/guide', function(req, res) {
  res.sendFile(path.join(__dirname, 'guide', 'index.html'));
});
app1.listen(8200);