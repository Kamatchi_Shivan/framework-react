import i18n from 'i18n-js'
import transformKeys from 'object-key-path-transformer'

import en from './languages/en.json'
import se from './languages/se.json'
import fr from './languages/fr.json'
import ar from './languages/ar.json'

export const lang = transformKeys(se)

export const setInitialLocale = () => {
  i18n.locale = "en"
}

i18n.defaultLocale = "en"
i18n.fallbacks = true
i18n.translations = { en, se, fr,ar }

export default i18n
