import React, {Component} from "react";
import {connect} from 'react-redux';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import '../../assets/styles/css/sidemenu.css';

class CaptchaImplementation extends Component {
    render() {
        return (
        <Card className="card-head">               
                <CardContent className="pg-inner-container">
                    <Typography gutterBottom variant="h5" component="h2" className="pg-title">Captcha Implementation</Typography>
                    <Typography variant="body2" component="p">Completely Automated Public Turing Tests to Tell Computers and Humans Apart (CAPTCHAs) are still one of the most efficient ways to prevent bots from spamming your website.</Typography> 
                    <Typography gutterBottom variant="h5" component="h3">What we provide</Typography>
                    <Typography variant="body2" component="p">As part of framework Captcha validation is support in the login process to prevent bot attacks. Framework configured in a way to choose multiple captcha platform available within the framework.</Typography>
                    <Typography gutterBottom variant="h5" component="h3">How we implement</Typography>
                    <Typography variant="body2" component="p">Captcha rendering is made as a configuration based rendering in the login UI. If captcha platform required is not available in the platform then it is implemented in the framework and configured to render.</Typography>
                    <Typography gutterBottom variant="h5" component="h3">How to use</Typography>
                    <Typography variant="body2" component="p">You can configure captcha platform you choose to use in the configuration file like (GOOGLE, YAHOO …etc) based on which framework will render the UI.</Typography>
                    <Typography gutterBottom variant="h5" component="h3">What to test</Typography>
                    <Typography variant="body2" component="p">Since captcha platform is already implemented in framework and we are just configuring which platform to use in rendering. We will not need to do any testing specific to this module.</Typography>
                </CardContent>       
        </Card>
        );
    }
}
const mapStateToProps = (state:any) => ({loginData:state.loginData,});
export default connect(mapStateToProps)(CaptchaImplementation);