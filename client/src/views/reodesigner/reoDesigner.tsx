import React, {Component} from "react";
import {connect} from 'react-redux';
import {Grid} from '@material-ui/core';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';


class REODesigner extends Component {
    render() {
        return (
            <Card className="card-head">              
                <CardContent className="pg-inner-container">
                    <Typography gutterBottom variant="h5" component="h2" className="pg-title">Real-time Enterprise Orchestration (REO) Designer</Typography>                                               
                    <img src={require("../../assets/images/REO_Designer.png")} alt="banner" className="reo-designer" />                    
                    <List component="ul">
                    <Typography variant="body2" component="p">REO is an orchestration tool built on top of NodeJS and ExpressJS. The features of the designer are as follows</Typography>
                        <ListItem>                            
                            <ListItemText primary="Rapid API development and management" />
                        </ListItem>
                        <ListItem>                          
                            <ListItemText primary="Create complex orchestrated APIs in matter of minutes" />
                        </ListItem>
                        <ListItem>                          
                            <ListItemText primary="By nature all APIs are asynchronous and better performing (NodeJS)" />
                        </ListItem>
                        <ListItem>                          
                            <ListItemText primary="Highly scalable at any level. You can individually scale an API or group of APIs (modules)" />
                        </ListItem>
                        <ListItem>                          
                            <ListItemText primary="Ease of use for developers and availability of reusable components" />
                        </ListItem>
                        <ListItem>                          
                            <ListItemText primary="Ability to custom components to library and publish it for others to consume" />
                        </ListItem>
                        <ListItem>                          
                            <ListItemText primary="Co-existance of API version and very less resource consumption" />
                        </ListItem>
                        <ListItem>                          
                            <ListItemText primary="Supports all cutting edge deployment frameworks and platforms" />
                        </ListItem>
                        <ListItem>                          
                            <ListItemText primary="Using DevOps pipeline staging and deployment is quick and 100% automated" />
                        </ListItem>
                    </List>   
                </CardContent>        
            </Card>                        
        );
    }
}
const mapStateToProps = (state:any) => ({loginData:state.loginData,});
export default connect(mapStateToProps)(REODesigner);