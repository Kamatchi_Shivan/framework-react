import React, {Component} from "react";
import {connect} from 'react-redux';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import '../../assets/styles/css/sidemenu.css';

class Authentication extends Component {  
    render() {
        return (
        <Card className="card-head">              
            <CardContent className="pg-inner-container">
                <Typography gutterBottom variant="h5" component="h2" className="pg-title">Authentication</Typography>
                <Typography variant="body2" component="p">Authentication is important as it enables organizations to keep their networks secure by permitting only authenticated users (or processes) to access its protected resources, which may include computer systems, networks, databases, websites, and other network-based applications or services.</Typography> 
                <Typography gutterBottom variant="h5" component="h3">What we provide</Typography>
                <img src={require("../../assets/images/Authentication.png")} alt="banner" className="img-responsive" />
                <Typography variant="body2" component="p">In Framework, Authentication / Authorization is enabled as a re-usable functionality implemented with best practices following SOLID principles. Authentication is enabled using DB based username password validation, LDAP lookup for validating a user, and ADFS based authentication. Once the user validated JWT token will be generated using a private key which will prevent security attacks</Typography>                
                <List component="ul">
                    <Typography gutterBottom variant="h5" component="h3">How we implement</Typography>
                    <ListItem>                            
                        <ListItemText primary="Configuration engine will look database for the type of authentication user/project configured to." />
                    </ListItem>
                    <ListItem>                          
                        <ListItemText primary="Based on the configuration, it will orchestrate towards corresponding flow to authenticate the user." />
                    </ListItem>
                    <ListItem>                          
                        <ListItemText primary="Once the user is successfully authenticated, user data will be used to generate JWT token based on a secured way of encryption and signing to prevent an attack." />
                    </ListItem>
                    <ListItem>                          
                        <ListItemText primary="Later the JWT token will be used for authorization in all API requests." />
                    </ListItem>
                </List>                                    
                <List component="ul">
                    <Typography gutterBottom variant="h5" component="h3">How to use</Typography>
                    <ListItem>                            
                        <ListItemText primary="To Use the Authentication module, we have to first go to the database based on what database we choose (MySql / MongoDB)." />
                    </ListItem>
                    <ListItem>                          
                        <ListItemText primary="We need to configure the type of authentication in the database." />
                    </ListItem>
                    <ListItem>                          
                        <ListItemText primary="Based on the type of authentication chosen, we need to provide the necessary configuration parameters. E.g.) In case of LDAP domain URL, username and password to authenticate, bind dn, search base…etc." />
                    </ListItem>                       
                </List>                     
                <Typography gutterBottom variant="h5" component="h3">What to test</Typography>
                <Typography variant="body2" component="p">Since the framework is tested already, when we consume the authentication module, we only need to test for configuration and appropriate URLs for DB / LDAP / ADFS.</Typography>
            </CardContent>        
        </Card>
        );
    }
}
const mapStateToProps = (state:any) => ({loginData:state.loginData,});
export default connect(mapStateToProps)(Authentication);