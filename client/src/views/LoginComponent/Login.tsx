import * as React from "react";
// import "./css/login.css"
// import "./css/font-awesome-4.7.0/css/font-awesome.css"
import "./js/api.js"
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import message from '../../components/messages/message.json'
import { loginAction, logoutAction} from '../../redux/actions/login/loginAction';
import  Captcha  from "./captcha";
import i18n, { lang} from '../../i18n'
import IconButton from '@material-ui/core/IconButton';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import EmailIcon from '@material-ui/icons/Email';
import LockIcon from '@material-ui/icons/Lock';
import { Grid, Container, Typography, withStyles,CardHeader,Card,CardContent, FormControl, 
  InputLabel, Input, InputAdornment, Button,MenuItem,Select } from '@material-ui/core';
import '../../assets/styles/css/login.css'

//const { Option } = Select;

type MyProps = {
  inmemoryLanguage: any,
  logoutAction: any,
  loginAction:any,
  loginData:any,
  form: any
};
interface document {
  [key:string]: any;
}

interface ILoginProps {
  //login?: (data: any) => void;
  classes?: any;
  inmemoryLanguage?: any,
  logoutAction?: any,
  loginAction?:any,
  //getMetaData?: any,
  loginData?:any,
  metaData?: any,
  form?: "values",
  getUserList?:any,
  getProjectList?:any,
  history?:any;
}

interface ILoginState {
  userName: string;
  password: string;
  showPassword:boolean;
  captchaError:string;
  language: string;
}

class Login extends React.Component<ILoginProps, ILoginState> {

  constructor(props:any){
    super(props)
      this.state = {
        userName: "",
        password: "",
        showPassword: false,
        captchaError:"",
        language: "en"
      }   
  }

  componentWillMount() {
    document.body.classList.add('login-pg');
  }

  private handleEmailAddressChange = (event: any) => {
    console.log("Email Target VAlue",event)
    this.setState({ userName: event.target.value })
  }

  private handlePasswordChange = (event: any) => {
      this.setState({ password: event.target.value })
  }

  private handleClickShowPassword = () => {
      this.setState({...this.state, showPassword:!this.state.showPassword});
  };

  private handleMouseDownPassword = (event:any) => {
    event.preventDefault();
  };
  
  handleSelect = (e:any) =>{
    console.log("hanfle select......", e.target.value)  
    if(e.target.value==='ar'){
      document.body.classList.add('arabic-lang');
      document.body.setAttribute('dir', 'rtl')
    }   
    else{
      document.body.classList.remove('arabic-lang');
      document.body.setAttribute('dir', 'ltr')
    } 
    localStorage.setItem("language",e.target.value);
    i18n.locale = e.target.value;
    this.setState({language:e.target.value})      
  };

    private handleLogin = () => {
      console.log("State",this.state)
      console.log("this.propsssssssss",this.props);
        let err;
        let values = this.state;
        if (!err) {
            let recaptcha = (document.forms as any)["myForm"]["g-recaptcha-response"].value;
            if (recaptcha === "") {
              // alert(" Required Captcha");
                //let captchaError=message.captchaErrorMessage;
                let captchaError= i18n.t(lang.error_messages["captcha_error_message"]);
                this.setState({captchaError});
                //this.setState({captchaError:message.captchaErrorMessage});
                //console.log("error",captchaError);
                return false;
            }
            else if(recaptcha!==""){
              let captchaError = "";
              this.setState({captchaError});
            }
            console.log('Received values of form: ', values);
            console.log('in loginnnnnn');
            this.props.loginAction(values);
            console.log(values);
        }
      // let values = this.state;
      // this.props.loginAction(values);
  }

  renderRedirect = () => {
    if (this.props.loginData.redirect) {
        return <Redirect to='/home'/>
    }
  }

  learnMoreClick = () => {   
    this.props.history.push(`/learnmore`);
  }

  // handlearabicClick = () => {  
  //   document.body.classList.add('arabic-lang');
  //   document.body.classList.remove('arabic-lang');
  // }

  public render(): JSX.Element {
      //let title = '';
    //   if(this.props.metaData.other_metadata){
    //     title = this.props.metaData.other_metadata.title;
    //     console.log(3000, title);
    //   }
      //const classes = this.props.classes;
      //const { getFieldDecorator } = this.props.form;
return (             
      <Grid className="login-layout" container direction="row">
        <Grid container className="header-section">                                        
          <FormControl variant="outlined" className="language-section">
            <Select defaultValue="en" className="language-dropdown" onChange = {this.handleSelect}>
              <MenuItem value="en">English</MenuItem>
              <MenuItem value="ar">Arabic </MenuItem>
              <MenuItem value="fr">French</MenuItem>
              <MenuItem value="se">Serbian</MenuItem>              
            </Select>
          </FormControl>
          <Grid item className="logo-section">              
              <img src ={require("./images/logo.png")} alt="banner" className="img-responsive" />
          </Grid>
        </Grid>
        <Grid container direction="row" className="login-container">              
          <Grid className="login">
            {this.renderRedirect()}
            <Grid className="login-content">
              <Grid className="login-content-section">
                <Grid className="login-content-section-top">
                  <Grid container direction="row">
                    <Typography variant="h5" component="h1" className="login-title">{i18n.t(lang.labels["welcome_to"])}<span> {i18n.t(lang.labels["expleo"])}</span> </Typography>                  
                  </Grid>
                  <Grid container direction="row">
                    <Typography variant="h5" component="h2" className="login-text">{i18n.t(lang.labels["on_off_boarding"])}</Typography>                  
                  </Grid>
                  <Grid container direction="row">
                    <Typography variant="h5" component="h2" className="login-para">{i18n.t(lang.labels["access_application_forever"])}</Typography>                  
                  </Grid>
                  <Grid container direction="row" className="login-learnmore-section"> 
                      <Typography variant="h5" component="h2" className="why-learn-more">{i18n.t(lang.labels["why_use_our_framework"])}</Typography> 
                      <Button variant="contained" color="primary" className="login-btn" onClick={this.learnMoreClick}>
                        {i18n.t(lang.buttons["learn_more"])}
                      </Button>                                    
                  </Grid>
                </Grid>
                <Grid className="login-content-section-bottom">
                  <img src={require("../../assets/images/React_Framework.jpg")} alt="banner" className="img-responsive" />
                </Grid>                
              </Grid> 
            </Grid> 
          </Grid>
          {/* Login form */}
          <Grid className="login-form">
            <Grid className="login-form-section">                  
              <Grid container direction="row">
                <Typography variant="h5" component="h1" className="login-content-title">{i18n.t(lang.labels["login"])}</Typography>                  
              </Grid>
              <Grid container direction="row">
                <Typography paragraph={true} className="login-content-info">{i18n.t(lang.labels["login_information"])}</Typography>                  
              </Grid>
              <Grid className="pg-login-form">
                <Grid className="login-form-box">
                  <Grid className="form-group mail-box">
                    <Grid container direction="row" className="form-info">
                      <FormControl fullWidth={true}>
                        <InputLabel htmlFor="email">{i18n.t(lang.labels["user_name"])}</InputLabel>
                        <Input
                            type="email"
                            value={this.state.userName}
                            onChange={this.handleEmailAddressChange}
                            id="email"
                            startAdornment={
                                <InputAdornment position="start">
                                    <EmailIcon className="email-icon"/>
                                </InputAdornment>}
                            required={true}
                        />
                      </FormControl>
                    </Grid>
                  </Grid>
                  <Grid className="form-group password-box">
                    <Grid container direction="row" className="form-info">
                      <FormControl fullWidth={true}>
                        <InputLabel htmlFor="password">{i18n.t(lang.labels["password"])}</InputLabel>
                        <Input
                            value={this.state.password}
                            onChange={this.handlePasswordChange}
                            type={this.state.showPassword ? 'text' : 'password'}
                            id="password"
                            required={true}
                            startAdornment={
                                <InputAdornment position="start">
                                    <LockIcon/>
                                </InputAdornment>}
                            endAdornment={
                              <InputAdornment position="end">
                                <IconButton  className="password-visibility"
                                  aria-label="toggle password visibility"
                                  onClick={this.handleClickShowPassword}
                                  onMouseDown={this.handleMouseDownPassword}
                                >
                                  {this.state.showPassword ? <Visibility /> : <VisibilityOff />}
                                </IconButton>
                              </InputAdornment>
                            }
                        />
                      </FormControl>
                    </Grid>
                </Grid>
                </Grid>

                  { (this.props.loginData.message) ? (
                  <Grid direction="row" justify="flex-start" alignItems="flex-start" className="loginerror">{this.props.loginData.message}</Grid>    
                  ) : (null)
                  }
                  
                <Grid container direction="row">
                  <Grid className="login-captcha">
                      <Captcha></Captcha>
                      { (this.state.captchaError) ? (
                        <Grid direction="row" justify="flex-start" alignItems="flex-start" className="loginerror">{this.state.captchaError}</Grid>    
                      ) : (null)
                    }                       
                  </Grid>                  
                </Grid>                
                <Grid container direction="row" justify="flex-start" alignItems="flex-end">
                  <Button variant="contained" color="primary" className="login-form-btn" onClick={this.handleLogin}>
                    {i18n.t(lang.buttons["login_now"])}
                  </Button>                  
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>

      </Grid>
    )
  }
}

const mapStateToProps = (state:any) => ({ loginData: state.loginData, metaData: state.metaData });

export default connect(mapStateToProps, { loginAction,logoutAction})((Login));
