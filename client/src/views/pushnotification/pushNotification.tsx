import React, {Component} from "react";
import {connect} from 'react-redux';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import '../../assets/styles/css/sidemenu.css';

class PushNotification extends Component {
    render() {
        return (
        <Card className="card-head">            
            <CardContent className="pg-inner-container">
                <Typography gutterBottom variant="h5" component="h2" className="pg-title">Push Notification</Typography>
                <Typography variant="body2" component="p">A push notification is an automated message that is sent to the user by the server of the app that’s working in the background (i.e., not open.) it is a message that’s displayed outside of the app.</Typography> 
                <Typography variant="body2" component="p">The Push Notification Framework updates the user interface with real time changes that have occurred on the server tier. The framework enables the PeopleSoft Internet Architecture to push data directly to the end user's display. ... Web Socket push technology on the web server.</Typography>
                <Typography gutterBottom variant="h5" component="h3">What we provide</Typography>
                <img src={require("../../assets/images/Push_Notification.png")} alt="banner" className="img-responsive" />
                <List component="ul">                  
                    <ListItem>                            
                        <ListItemText primary="It is highly scalable and highly secured. " />
                    </ListItem>
                    <ListItem>                          
                        <ListItemText primary="Application agnostic." />
                    </ListItem>
                    <ListItem>                          
                        <ListItemText primary="Push notification can accept services from multiple applications. " />
                    </ListItem>
                    <ListItem>                          
                        <ListItemText primary="Push notification can receive subscription from multiple applications concurrently." />
                    </ListItem>
                </List>     
                <Typography variant="body2" component="p">Push notification for web application. Push notification service will broadcast the message based on the user subscription. If the user does not subscribe for the notification they will not receive notification from push notification service.</Typography>                             
                <List component="ul">
                    <Typography gutterBottom variant="h5" component="h3">How we implement</Typography>
                    <ListItem>                            
                        <ListItemText primary="Web socket is used for sending push notification from Server (Push notification service)." />
                    </ListItem>
                    <ListItem>                          
                        <ListItemText primary="Subscription details are stored in the database for individual users." />
                    </ListItem>                        
                </List>                                   
                <List component="ul">
                    <Typography gutterBottom variant="h5" component="h3">How to use</Typography>
                    <ListItem>                            
                        <ListItemText primary="Push notification service is exposed as a micro service." />
                    </ListItem>
                    <ListItem>                          
                        <ListItemText primary="If someone wants to push notification to their clients. They can invoke the micro service to get it done." />
                    </ListItem>
                    <ListItem>                          
                        <ListItemText primary="If someone wants to manage subscription to push notification, they can modify the configuration in the database." />
                    </ListItem>                          
                </List>                                    
                <List component="ul">
                    <Typography gutterBottom variant="h5" component="h3">What to test</Typography>
                    <ListItem>                            
                        <ListItemText primary="User subscription based on the project needs to be tested." />
                    </ListItem>                                                 
                </List>                    
            </CardContent>        
        </Card>
        );
    }
}
const mapStateToProps = (state:any) => ({loginData:state.loginData,});
export default connect(mapStateToProps)(PushNotification);