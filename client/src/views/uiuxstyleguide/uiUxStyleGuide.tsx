import React, {Component} from "react";
import {connect} from 'react-redux';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import '../../assets/styles/css/sidemenu.css';

class UiUxStyleGuide extends Component {
    render() {
        return (
        <Card className="card-head">              
            <CardContent className="pg-inner-container">
                <Typography gutterBottom variant="h5" component="h2" className="pg-title">UI Style Guide</Typography>
                <Typography variant="body2" component="p"><a href="http://localhost:8200/" target="_blank">Click Here</a> to view UI Style Guide</Typography>                    
            </CardContent>          
        </Card>
        );
    }
}
const mapStateToProps = (state:any) => ({loginData:state.loginData,});
export default connect(mapStateToProps)(UiUxStyleGuide);