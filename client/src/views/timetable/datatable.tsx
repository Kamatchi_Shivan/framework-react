import React from "react";
import {connect} from 'react-redux';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import '../../assets/styles/css/sidemenu.css';
import {getDataTableList} from '../../redux/actions/dataTable/dataTableAction'
import {config} from '../../config/configs'
import { CLIENT_RENEG_LIMIT } from "tls";

interface MyProps{
    getDataTableList?:any,
    dataList?:any
}

interface MyState{
    page:any;
    rowsPerPage:any;
    setPage:any;
    setRowsPerPage:any;
    order:String;
    orderBy:String;
}

function descendingComparator(a:any, b:any, orderBy:any) {
    if (b[orderBy] < a[orderBy]) {
      return -1;
    }
    if (b[orderBy] > a[orderBy]) {
      return 1;
    }
    return 0;
  }

function getComparator(order:any, orderBy:any) {
    return order === 'desc'
      ? (a:any, b:any) => descendingComparator(a, b, orderBy)
      : (a:any, b:any) => -descendingComparator(a, b, orderBy);
  }

function stableSort(array:any, comparator:any) {
    const stabilizedThis = array.map((el:any, index:any) => [el, index]);
    stabilizedThis.sort((a:any, b:any) => {
      const order = comparator(a[0], b[0]);
      if (order !== 0) return order;
      return a[1] - b[1];
    });
    return stabilizedThis.map((el:any) => el[0]);
  }

const StyledTableCell = withStyles((theme) => ({
    head: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    },
  }))(TableCell);

class DataTable extends React.Component<MyProps,MyState> {
    constructor(props:any){
        super(props);
        this.state={
            page:0,
            rowsPerPage:5,
            setPage:0,
            setRowsPerPage:5,
            order:'asc',
            orderBy:this.props.dataList.STUDENT_NAME
        }
    }

    componentWillMount(){
        this.props.getDataTableList(config.dataTableListURL);
    }

    handleChangePage = (event:any,newPage:any)=>{
        console.log("New Page",newPage)
        this.setState({
            page:newPage
        })
    }

    handleChangeRowsPerPage=(event:any)=>{
        let rowPage=parseInt(event.target.value,10);
        this.setState({
            rowsPerPage:rowPage,
            page:0
        })
    }

    render() {
        console.log("dataList", this.props.dataList)
        let page=this.state.page;        
        let rowsPerPage=this.state.rowsPerPage;
        if(this.props.dataList){
        return (           
            <Card className="card-head"> 
                <CardContent className="pg-inner-container">
                    <Typography gutterBottom variant="h5" component="h2" className="pg-title">Data Table</Typography>
                    <TableContainer component={Paper} className="emp-list">
                        <Table className="emp-table clone-req">
                            <TableHead>
                                <TableRow>
                                    <StyledTableCell>Column 1</StyledTableCell>
                                    <StyledTableCell>Column 2</StyledTableCell>
                                    <StyledTableCell>Column 3</StyledTableCell>
                                    <StyledTableCell>Column 4</StyledTableCell>                            
                                </TableRow>
                            </TableHead>
                            <TableBody>
                            {stableSort(this.props.dataList, getComparator(this.state.order, this.state.orderBy))
                            .slice(this.state.page * this.state.rowsPerPage, this.state.page * this.state.rowsPerPage + this.state.rowsPerPage)
                            .map((row:any,index:any)=>{ 
                                const labelId = `enhanced-table-checkbox-${index}`;
                                console.log("Page",this.state.page)
                                console.log("RowsPerpage",this.state.rowsPerPage) 
                                console.log("IN Next",row.STUDENT_NAME)                             
                               return(
                                   <TableRow
                                   hover
                                   key={row.SNo}
                                   >
                                    <TableCell>{row.SNo}</TableCell>
                                    <TableCell>{row.TEACHER_NAME}</TableCell>
                                    <TableCell component="th" id={labelId} scope="row">{row.STUDENT_NAME}</TableCell>
                                    <TableCell>{row.SUBJECT}</TableCell>
                                   </TableRow>
                               )
                            })
                                // this.props.dataList.map((data:any)=>{
                                // return(
                                //     <TableRow key={data.SNo}>
                                //         <TableCell>{data.SNo}</TableCell>
                                //         <TableCell>{data.TEACHER_NAME}</TableCell>
                                //         <TableCell>{data.STUDENT_NAME}</TableCell>
                                //         <TableCell>{data.SUBJECT}</TableCell>
                                //     </TableRow>
                                //     )
                                // })
                            }
                            </TableBody>
                        </Table>
                    </TableContainer>
                    <TablePagination
                        rowsPerPageOptions={[5, 10, 15]}
                        component="div"
                        count={this.props.dataList.length}
                        rowsPerPage={this.state.rowsPerPage}
                        page={this.state.page}
                        onChangePage={(event:any,newPage:any)=>this.handleChangePage(event,newPage)}
                        onChangeRowsPerPage={this.handleChangeRowsPerPage}
                    />
                </CardContent>
            </Card> 
        );
        }
    }
}
const mapStateToProps = (state:any) => ({
    loginData:state.loginData,
    dataList:state.dataTable.dataList
});
export default connect(mapStateToProps,{getDataTableList})(DataTable);