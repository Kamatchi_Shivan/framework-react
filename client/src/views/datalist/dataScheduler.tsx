import React, { Component } from 'react'
import DataList from '../datalist/dataList' // Importing Component Here 
import {connect} from 'react-redux'
import {getDataTableList} from '../../redux/actions/dataTable/dataTableAction'
import {config} from '../../config/configs'

type MyProps = {
    getDataTableList: any,
    dataList: any
}

 class DataScheduler extends Component<MyProps> {

    componentWillMount(){
        this.props.getDataTableList(config.dataTableListURL);
    }    
    
    render() {
        console.log("dataaa",this.props.dataList)
        return (
            <DataList value = {this.props.dataList}></DataList> // Parent component Passing Values in props here  // Passing Values fro redux
        )
    }
}

const mapStateToProps = (state: any) => ({
    dataList:state.dataTable.dataList
});
  
  
export default connect( mapStateToProps,{getDataTableList} )(DataScheduler);

