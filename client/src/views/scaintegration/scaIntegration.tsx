import React, {Component} from "react";
import {connect} from 'react-redux';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import '../../assets/styles/css/sidemenu.css';

class SCAIntegration extends Component {
    render() {
        return (
        <Card className="card-head">               
            <CardContent className="pg-inner-container">
                <Typography gutterBottom variant="h5" component="h2" className="pg-title">Sonar Qube Integration</Typography>
                <Typography variant="body2" component="p">SonarQube Integration is an open source static code analysis tool that is gaining tremendous popularity among software developers. SonarQube enables developers to track code quality, which helps them to ascertain if a project is ready to be deployed in production.</Typography> 
                <Typography variant="body2" component="p">With continuous Code Quality SonarQube will enhance your workflow through automated code review, CI/CD integration, pull requests decorations</Typography>
                <Typography variant="body2" component="p"><a href="http://localhost:9000/" target="_blank">Click Here</a> to view Sonarqube output</Typography>
                <Typography gutterBottom variant="h5" component="h3">What we provide</Typography>
                <Typography variant="body2" component="p">For every PR raised, we will be running the sonar qube scanner and based on the output Reviewer will merge the code thus ensuring code quality and all security threats at code level will be taken care.</Typography>
                <Typography gutterBottom variant="h5" component="h2" className="pg-title">LINT Configuration</Typography>
                <Typography variant="body2" component="p">Linting is the process of running your code through a tool to analyse for potential errors. Linting isn’t language specific and can be customised on a per project basis to ensure code quality, improved performance, reduce developer decision making fatigue and consistency.</Typography>               
            </CardContent>          
        </Card>
        );
    }
}
const mapStateToProps = (state:any) => ({loginData:state.loginData,});
export default connect(mapStateToProps)(SCAIntegration);