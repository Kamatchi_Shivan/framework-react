import React from 'react';
import {connect} from 'react-redux';
import { CardActionArea, Grid, Box, Card, CardMedia, Icon } from '@material-ui/core';
import onBoarding from '../../assets/Icons/SVG/onboarding.svg';
import offBoarding from '../../assets/Icons/SVG/offboard-svg-lg.svg';
import modification from '../../assets/Icons/SVG/modification.svg';
import dashboard from '../../assets/Icons/SVG/dashboard.svg';
import setting from '../../assets/Icons/SVG/setting.svg';
import '../../assets/styles/css/dashboard.css'

interface MyProps {
    history?:any;
    loginData?:any;
}


class Dashboard extends React.Component<MyProps,{}> {

    constructor(props:any){
        super(props);     
        
    }
   
    uiuxClick =()=>{
        this.props.history.push(`/uiuxstyleguide`);
    }

    authenticationClick =()=>{
        this.props.history.push(`/authentication`);
    }

    pushnotificationClick =()=>{
        this.props.history.push(`/pushnotification`);
    }

    captchaimplementationClick =()=>{
        this.props.history.push(`/captchaimplementation`);
    }

    loggingerrorhandlingClick =()=>{
        this.props.history.push(`/loggingerrorhandling`);
    }

    scaintegrationClick =()=>{
        this.props.history.push(`/scaintegration`);
    }

    reodesignerClick =()=>{
        this.props.history.push(`/reodesigner`);
    }

    dataTableClick =()=>{
        this.props.history.push(`/datatable`);
    }

    render(){
        return(
           
            <Box className="box-layout">                
                <Grid className="dashboard-layout" container>
                    <Grid item xs={12} sm={6} md={4} lg={3} xl={3} >
                        <Card className="dashboard-grid" onClick={this.uiuxClick}> 
                            <CardActionArea className="grid-item  uiuxstyle-bg">
                                <Grid item>
                                    <Icon > <img alt="UI/UXStyleGuide" src={onBoarding}/> </Icon>
                                </Grid>
                                <Grid item >
                                    <h3>UI Style Guide</h3>
                                </Grid>
                            </CardActionArea>
                        </Card>
                    </Grid>
                    <Grid item xs={12} sm={6} md={4} lg={3} xl={3} >
                        <Card className="dashboard-grid" onClick={this.authenticationClick}> 
                            <CardActionArea className="grid-item  authentication-bg">
                                <Grid item>
                                    <Icon > <img alt="Authentication" src={offBoarding}/> </Icon>
                                </Grid>
                                <Grid item >
                                    <h3>Authentication</h3>
                                </Grid>
                            </CardActionArea>
                        </Card>
                    </Grid>
                    <Grid item xs={12} sm={6} md={4} lg={3} xl={3} >
                        <Card className="dashboard-grid" onClick={this.pushnotificationClick}> 
                            <CardActionArea className="grid-item  pushnotification-bg">
                                    <Grid item>
                                        <Icon > <img alt="Push Notification" src={modification}/> </Icon>
                                    </Grid>
                                    <Grid item >
                                        <h3>Push Notification</h3>
                                    </Grid>
                            </CardActionArea>
                        </Card>
                    </Grid>
                    <Grid item xs={12} sm={6} md={4} lg={3} xl={3} >
                        <Card className="dashboard-grid" onClick={this.captchaimplementationClick}> 
                            <CardActionArea className="grid-item  captchaimplementation-bg">
                                <Grid item>
                                    <Icon > <img alt="Captcha Implementation" src={dashboard}/> </Icon>
                                </Grid>
                                <Grid item >
                                    <h3>Captcha Implementation</h3>
                                </Grid>
                            </CardActionArea>
                        </Card>
                    </Grid>
                    <Grid item xs={12} sm={6} md={4} lg={3} xl={3} >
                        <Card className="dashboard-grid" onClick={this.loggingerrorhandlingClick}> 
                            <CardActionArea className="grid-item errorhandling-bg">
                                <Grid item>
                                    <Icon > <img alt="Error Handling" src={onBoarding}/> </Icon>
                                </Grid>
                                <Grid item >
                                    <h3>Logging & Error Handling</h3>
                                </Grid>
                            </CardActionArea>
                        </Card>
                    </Grid>
                    <Grid item xs={12} sm={6} md={4} lg={3} xl={3} >
                        <Card className="dashboard-grid" onClick={this.scaintegrationClick}> 
                            <CardActionArea className="grid-item scaintegration-bg">
                                <Grid item>
                                    <Icon > <img alt="SCA Integration" src={offBoarding}/> </Icon>
                                </Grid>
                                <Grid item >
                                    <h3>SCA Integration</h3>
                                </Grid>
                            </CardActionArea>
                        </Card>
                    </Grid>
                    <Grid item xs={12} sm={6} md={4} lg={3} xl={3} >
                        <Card className="dashboard-grid" onClick={this.reodesignerClick}> 
                            <CardActionArea className="grid-item reodesigner-bg">
                                <Grid item>
                                    <Icon > <img alt="REO Designer" src={setting}/> </Icon>
                                </Grid>
                                <Grid item >
                                    <h3>REO Designer</h3>
                                </Grid>
                            </CardActionArea>
                        </Card>
                    </Grid>
                    
                    <Grid item xs={12} sm={6} md={4} lg={3} xl={3} >
                        <Card className="dashboard-grid" onClick={this.dataTableClick}> 
                            <CardActionArea className="grid-item datatable-bg">
                                <Grid item>
                                    <Icon > <img alt="Time Table" src={modification}/> </Icon>
                                </Grid>
                                <Grid item >
                                    <h3>Data Table</h3>
                                </Grid>
                            </CardActionArea>
                        </Card>
                    </Grid>                    

                </Grid>  
            </Box>            
        )
    }
}

const mapStateToProps = (state:any) => ({
    loginData:state.loginData
  });

export default connect(mapStateToProps,{}) (Dashboard);



