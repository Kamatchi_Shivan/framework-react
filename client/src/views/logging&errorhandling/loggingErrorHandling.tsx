import React, {Component} from "react";
import {connect} from 'react-redux';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import '../../assets/styles/css/sidemenu.css';

class LoggingErrorHandling extends Component {
    render() {
        return (
        <Card className="card-head">               
            <CardContent className="pg-inner-container">
                <Typography gutterBottom variant="h5" component="h2" className="pg-title">Logging &amp; Error Handling</Typography>
                <Typography variant="body2" component="p">Logging is an essential component of any application, especially when a critical error occurs. Whether or not you recover from an exception, logging the problem can help you identify the cause of – and ultimately the solution to – potential problems in your application. Logging close to the source of the error provides as much detail about the error as possible.</Typography> 
                <Typography variant="body2" component="p">Error handling refers to the anticipation, detection, and resolution of programming, application, and communications errors. ... Logic errors, also called bugs, occur when executed code does not produce the expected or desired result. Logic errors are best handled by meticulous program debugging.</Typography>
                <Typography variant="body2" component="p">Error handling is important because it makes it easier for the end users of your code to use it correctly. ... If you don't handle your errors, your program may crash, lose all of your customer’s work and you likely won't know where the bug occurred.</Typography>
                <Typography variant="body2" component="p">Syntax errors, which are typographical mistakes or improper use of special characters, are handled by rigorous proofreading. Logic errors, also called bugs, occur when executed code does not produce the expected or desired result.Logic errors are best handled by meticulous program debugging.</Typography>
                <Typography gutterBottom variant="h5" component="h3">What we provide</Typography>
                <Typography variant="body2" component="p">For Error handling, we provide generalized and standard way of handling errors in both client and server tier.</Typography>
                <Typography variant="body2" component="p">In backend, all the errors will be reffered as key in the flow and using generalized implementation, we will look into database / in memory database to fetch error message, user message, error severity based on key and language code.In front end, all the errors will be retrieved from common json file.</Typography>
                <Typography gutterBottom variant="h5" component="h3">How we implement</Typography>
                <Typography gutterBottom variant="h5" component="h3">Logging:</Typography>
                <Typography variant="body2" component="p">Front End: We have used log4js for logging the error, info and warning in the file and can be extended to log the same information in the database with basic information like timestamp, request details and few other details.</Typography>
                <Typography variant="body2" component="p">Back End:In designer, we have extended the logger node to make it as configurable for user to choose logger level (INFO, DEBUG, WARN) and also they can choose to log at File, Database or console wherever applicable in the flow.</Typography>
                <Typography gutterBottom variant="h5" component="h3">How to use</Typography>
                <Typography gutterBottom variant="h5" component="h3">Logging:</Typography>
                <Typography variant="body2" component="p">Developer can drag and drop logger node from pallete and inject in flow and choose options best suits for the need.</Typography>
                <Typography gutterBottom variant="h5" component="h3">Error Handling:</Typography>
                <Typography variant="body2" component="p">Developer can configure the error key in the database if it is not available already based on the language code and use Error hanlding sub flow in the flow and pass error key as the input along with the language code.</Typography>
                <Typography gutterBottom variant="h5" component="h3">What to test</Typography>
                <Typography variant="body2" component="p">Developer can only test the error configuration in database and logging configuration they have done in the flow rest will be covered as part of the framework.</Typography>                
            </CardContent>         
        </Card>
        );
    }
}
const mapStateToProps = (state:any) => ({loginData:state.loginData,});
export default connect(mapStateToProps)(LoggingErrorHandling);