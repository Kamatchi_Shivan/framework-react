import React, {Component} from "react";
import {connect} from 'react-redux';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { Grid, Button, Container} from '@material-ui/core';
import i18n, { lang} from '../../i18n'
import '../../assets/styles/css/sidemenu.css';

interface LearnProps {    
    history?:any;
  }

class LearnMore extends React.Component<LearnProps,{}> {

    backLogin = () => {   
        this.props.history.push(`/login`);
    }

    componentWillMount() {
      document.body.classList.add('learnmore-pg');
    }
    
    render() {
        return (
        <Card className="card-head learn-more-pg">               
            <CardContent className="pg-inner-container">
                <Typography gutterBottom variant="h5" component="h2" className="pg-title">Why use our Framework?</Typography>
                <Typography variant="body2" component="p">A well orchestrated modular platform built using the best industry standard frameworks like MEAN, MERN and Mobile platform</Typography>
                <Typography variant="body2" component="p">All components developed are built keeping SOLID principles and modularity as its core. Each component and features are built by plug-n play architecture hence it is easy to customize or upgrade individual features (ie) Captcha, Notification, Authentication, UI library etc</Typography> 
                <Typography variant="body2" component="p">Building rapid applications using pre-built features such as Authentication, Authorization, User management and Role management</Typography> 
                <Typography variant="body2" component="p">Using the modular framework and factory pattern we can achieve ZERO downtime. Restarts are needed only when adding new features or change in core logic</Typography> 
                <Typography variant="body2" component="p">UI Layout, Navigation, Data management from page to page, Unit testing and Security testing are pre-built. The developers can start with a robust platform which gives them a head boost into application development</Typography>
                <Typography variant="body2" component="p">REO Designer, is a javascript based service orchestration tool. Designing complex API's can be done in matter of minutes using a very intutive UI with drag and drop features</Typography>    
                <Grid container>
                  <Button variant="contained" color="primary" className="login-form-btn learn-more-btn" onClick={this.backLogin}>
                    {i18n.t(lang.buttons["back_home"])}
                  </Button>                  
                </Grid>
            </CardContent>         
        </Card>
        );
    }
}
const mapStateToProps = (state:any) => ({loginData:state.loginData,});
export default connect(mapStateToProps)(LearnMore);