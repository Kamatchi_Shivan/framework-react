import { CHANGE_PASSWORD } from "../types";
import fetch from '../../../utils/leoFetch'
import axios from '../../../utils/leoAxios'
import {config} from './../../../config/configs'
 
export const changePassword = (values:any) => (dispatch:any) => {
    console.log("Values in action",values,);
    let payload = {
        userName:values.email,
        password: values.confirmPassword
    }
    console.log("Inside Update Password...",payload)
    axios.post(config.apiRootPath+config.changePasswordURL, payload,  {
      headers: {
          'Content-Type': 'application/json',
      }
    })
    .then(res => {
        console.log("In Res Dispatch")
           dispatch({
               payload:{
                   ...res.data
               },
               type: CHANGE_PASSWORD
           })
        })
  };