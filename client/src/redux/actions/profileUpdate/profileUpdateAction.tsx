import { UPDATE_PROFILE } from "../types";
import fetch from '../../../utils/leoFetch'
import axios from '../../../utils/leoAxios'
import { config } from "../../../config/configs";
 
export const updateUser = (username:any,status:any,values:any) => (dispatch:any) => {
    console.log("Values in action",values, "Username",username);
    let payload = {
        userName:username,
        status:status,
        firstName: values.firstName,
        middleName: values.middleName,
        lastName: values.lastName
    }
    console.log("Inside Update user...",payload)
    axios.post(config.apiRootPath+config.updateUserURL, payload,  {
      headers: {
          'Content-Type': 'application/json',
      }
    })
    .then(res => {
        console.log("In Res Dispatch")
           dispatch({
               payload:{
                   ...res.data
               },
               type: UPDATE_PROFILE
           })
        })
  };