import { LANGUAGE,DATA_TABLE_LIST } from '../types';
import fetch from '../../../utils/leoFetch'
//import axios from '../../../utils/leoAxios'
import { config } from "../../../config/configs";

/* Select Language */
export const inmemoryLanguage = (objData:any) => (dispatch:any) => {
    console.log("inmemoryLanguage action......", objData);
    dispatch({
      payload: objData,
      type: LANGUAGE
    });
  };

export const getDataTableList = (timetableurl:any) => (dispatch:any) => {
    console.log(100,config.apiRootPath + config.dataTableListURL)
    let options={}
    fetch(config.apiRootPath + timetableurl, options)
    .then(res => res.json())
    .then(dataList =>
      dispatch({
        payload: dataList,
        type: DATA_TABLE_LIST
      })
    );
  };