import { LANGUAGE,DATA_TABLE_LIST } from '../../actions/types';

const initialState = {
    language : 'en',
    dataList:[]
}

export default function(state = initialState, action:any) {
    switch (action.type) {
        case LANGUAGE:
            return {
                ...state,
                language: action.payload
            };
        case DATA_TABLE_LIST:
            return{
                ...state,
                dataList:action.payload
            }
        default:
            return state;
    }
}
