import { META_DATA } from '../../actions/types';

const initialState = {
    Email: null,
    isLoggedIn: false,
    redirect: false,
    token: null,
}

export default function(state = initialState, action:any) {
    switch (action.type) {
        case META_DATA:
            return {
                ...action.payload
            };
        default:
            return state;
    }
}
