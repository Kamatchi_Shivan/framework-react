import React from 'react';
import {Provider} from 'react-redux';
import {BrowserRouter as Router, Route} from "react-router-dom";
import withTracker from "./withTracker";
import routes from "./routes";
import {config} from './config/configs'
import store from './store';
import './App.css'


function App() {
  return (
    <Provider store={store}>
    <Router basename={config.appBaseName || ""}>
        <div>
            {routes.map((route, index) => {
                return (
                    <Route
                        key={index}
                        path={route.path}
                        exact={route.exact}
                        component={withTracker((props:any) => {
                        return (
                            <route.layout {...props}>
                                <route.component {...props}/>
                            </route.layout>
                        );
                    })}/>
                );
            })}
        </div>
    </Router>
</Provider>
  );
}

export default App;
