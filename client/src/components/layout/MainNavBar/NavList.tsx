import React from 'react';
import {connect} from 'react-redux';
import { CssBaseline,ListItemIcon,ListItemText,List,Grid,Icon} from '@material-ui/core';
import PowerSettingsNewIcon from '@material-ui/icons/PowerSettingsNew';
import ListItem, { ListItemProps } from '@material-ui/core/ListItem';
import '../../../assets/styles/css/navlist.css';
import OnBoardingIcon from '../../../assets/Icons/SVG/onboarding-sidebar-icon.svg';
import OffBoardingIcon from '../../../assets/Icons/SVG/offboard-sidebar-icon.svg';
import ModifyProvisionIcon from '../../../assets/Icons/SVG/modification-sidebar-icon.svg';
import DashboardIcon from '../../../assets/Icons/SVG/dashboard-sidebar-icon.svg';
import SettingIcon from '../../../assets/Icons/SVG/settings-sidebar-icon.svg';
import { Link } from 'react-router-dom';

function ListItemLink(props: ListItemProps<'a', { button?: true }>) {
  return <ListItem button component="a" {...props} />;
}

export default  () => {

 //Sidemenu display condition bases on URL
  let navigationURl;
  let myLoc = JSON.stringify(window.location.pathname);
  let finalstr = myLoc.replace(/"/g, "");
  navigationURl = finalstr.substring(finalstr.lastIndexOf("/") + 1);

  //Menu open
  const [open, setOpen] = React.useState(false);
  const [openr, setOpenr] = React.useState(false);

  
  
  return (
    <div className="navlist-layout">
      <CssBaseline />
      <Grid container   direction="row"  justify="flex-start"  alignItems="flex-start"  >
        <Grid item >
        {navigationURl === "home" ? (
            <List>
            {['Logout'].map((text, index) => (
                <ListItem button key={text}>
                  <ListItemLink href="/login" className="listicon-logout">
                  <ListItemIcon className="listicon">
                    <PowerSettingsNewIcon/>
                  </ListItemIcon>
                   <ListItemText primary={text} />
                  </ListItemLink> 
                </ListItem>
            ))}
           </List>
           ) : (            
            <List className="sidemenu-list">
              {['UI Style Guide'].map((text, index) => (                
                  <ListItem button key={text}>
                    <ListItemIcon className="listicon"><Link to='/uiuxstyleguide'><img alt="Onboarding" className="navlist-icon" src={OnBoardingIcon}/></Link></ListItemIcon>   
                    <ListItemLink>
                      <Link to='/uiuxstyleguide'>
                      <ListItemText primary={text}/>
                      </Link>
                    </ListItemLink> 
                  </ListItem>
              ))}
              

              {['Authentication'].map((text, index) => (
                  <ListItem button key={text}>
                    <ListItemIcon className="listicon"><Link to="/authentication"><img alt="Offboarding" className="navlist-icon" src={ModifyProvisionIcon}/></Link></ListItemIcon> 
                    <ListItemLink>     
                      <Link to="/authentication">              
                      <ListItemText primary={text} />
                      </Link>
                    </ListItemLink> 
                  </ListItem>
              ))}

              {['Push Notification'].map((text, index) => (
                  <ListItem button key={text}>
                    <ListItemIcon className="listicon"><Link to="/pushnotification"><img alt="ModifyProvision" className="navlist-icon" src={OffBoardingIcon}/></Link></ListItemIcon> 
                    <ListItemLink>     
                      <Link to="/pushnotification">               
                      <ListItemText primary={text} />
                      </Link>
                    </ListItemLink> 
                  </ListItem>
              ))}

              {['Captcha Implementation'].map((text, index) => (
                  <ListItem button key={text}>
                    <ListItemIcon className="listicon"><Link to="/captchaimplementation"><img alt="Dashboard" className="navlist-icon" src={DashboardIcon}/></Link></ListItemIcon> 
                    <ListItemLink>
                      <Link to="/captchaimplementation">
                      <ListItemText primary={text} />
                      </Link>
                    </ListItemLink> 
                  </ListItem>
              ))}

              {['Logging & Error Handling'].map((text, index) => (
                  <ListItem button key={text}>
                    <ListItemIcon className="listicon"><Link to="/loggingerrorhandling"><img alt="Setting" className="navlist-icon" src={OnBoardingIcon}/></Link></ListItemIcon> 
                    <ListItemLink>      
                      <Link to="/loggingerrorhandling">              
                      <ListItemText primary={text} />
                      </Link>
                    </ListItemLink> 
                  </ListItem>
              ))}

              {['SCA Integration'].map((text, index) => (
                  <ListItem button key={text}>
                    <ListItemIcon className="listicon"><Link to="/scaintegration"><img alt="Setting" className="navlist-icon" src={ModifyProvisionIcon}/></Link></ListItemIcon> 
                    <ListItemLink> 
                      <Link to="/scaintegration">                   
                      <ListItemText primary={text} />
                      </Link>
                    </ListItemLink> 
                  </ListItem>
              ))}

              {['REO Designer'].map((text, index) => (
                  <ListItem button key={text}>
                    <ListItemIcon className="listicon"><Link to="/reodesigner"><img alt="Setting" className="navlist-icon" src={SettingIcon}/></Link></ListItemIcon> 
                    <ListItemLink>    
                      <Link to="/reodesigner">                
                      <ListItemText primary={text} />
                      </Link>
                    </ListItemLink> 
                  </ListItem>
              ))}
            
              {['Logout'].map((text, index) => (
                  <ListItem button key={text}>
                    <ListItemLink href="/login" className="listicon-logout">
                    <ListItemIcon className="listicon sidebard-logout">
                      <PowerSettingsNewIcon/>
                    </ListItemIcon>
                    <ListItemText primary={text} />
                    </ListItemLink> 
                  </ListItem>
              ))}           

              

            </List> 
            )       
          }
          </Grid>
        </Grid>
      </div>
  );
}

