  import React from 'react';
  import clsx from 'clsx';  
  import { CssBaseline,Box, Divider, IconButton,Grid,  Drawer} from '@material-ui/core';
  import '../../../assets/styles/css/drawer.css';   
  /**Imports of Navigation list in Drawer component */
  import NavList from '../MainNavBar/NavList'

  export default  () => {    
    const [open, setOpen] = React.useState(false);
    
  /**Toggle Drawer component */
    const toggle = React.useCallback(
      () => setOpen(!open),
      [open, setOpen],
    );
    // const handleClose = () => {
    //   setOpen(false);
    // };
    return (
      <div className="drawer-layout">
        <CssBaseline />
          <Grid container   direction="row"  justify="flex-start"  alignItems="flex-start"  >
            <Grid item  className="drawer-close">
            {/**Drawer component */}
              <Box >
                <Drawer variant="permanent"  
                        className={clsx("drawer", {
                        ["drawer-open"]: open,
                        ["drawer-close"]: !open,
                      })}
                      classes={{
                        paper: clsx({
                          ["drawer-open"]: open,
                          ["drawer-close"]: !open,
                        }),
                      }}
                    >
                      <div className="toolbar">
                        {open ? 
                          <IconButton  onClick={toggle} className="logo-large" disableRipple>
                                <img  src={require("../../../assets/images/WhiteLogo_exp.png")}
                                  alt="Onboarding Dashboard"
                                />
                          </IconButton>
                          :
                          <IconButton  onClick={toggle}  disableRipple>
                                <img  className="logo-small" src={require("../../../assets/images/White_Logo.png")}
                                  alt="Onboarding Dashboard"
                                />
                          </IconButton>
                        }
                      </div>
                      <Divider />
                      {/**Navigation List component  */}                      
                      <NavList/>
                  </Drawer>
                  </Box>
                </Grid>
              </Grid>
            </div>
          );
      }


