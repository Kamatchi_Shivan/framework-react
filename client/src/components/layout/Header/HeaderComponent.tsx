import React from 'react';
import {connect} from 'react-redux';
//import CssBaseline from '@material-ui/core/CssBaseline';
import { Grid } from '@material-ui/core';
import {Badge,Typography,AppBar,Toolbar,Icon} from '@material-ui/core';
import NotificationsIcon from '@material-ui/icons/Notifications';
import UserIconComponent from './UserIconComponent';
import userIcon from '../../../assets/Icons/SVG/user-icon.svg';
import Notifications from '../../layout/MainNavBar/Notifications'
import '../../../assets/styles/css/header.css';

//Sidemenu display condition bases on URL
let navigationURlHeader:any;
let  userRole:any ;
interface MyProps {
    loginData?:any,
    classes?:any
}

interface MyState{
    notification:boolean;    
}


class HeaderComponent extends React.Component<MyProps,MyState> {

    constructor(props:any) {
        super(props); 
        this.state={
            notification:false,                       
        }       
    }

    handleNotificationOpen = () => {
        this.setState({
            notification:true                           
        })
    };

    componentWillMount() {
        console.log("loginData:>>>>>>>", this.props.loginData);
    }

    render(){
        let myLoc = JSON.stringify(window.location.pathname);
        let finalstr = myLoc.replace(/"/g, "");
        navigationURlHeader = finalstr.substring(finalstr.lastIndexOf("/") + 1);
        
        return(
            <Grid container direction="row" justify="flex-start" alignItems="stretch" >
                <AppBar className="header-layout">
                    <Toolbar className="toolbar">
                        <Grid item className="toolbar-container">
                            <Grid container  direction="row" justify="space-between" alignItems="center" >                               
                                <Grid item>
                                    <Typography variant="h5" className="header-content"> Welcome to Expleo Framework </Typography>
                                </Grid>                                                             
                                <Grid item >
                                    <Grid container direction="row" justify="flex-end" alignItems="center"  >
                                        <Grid item className="nodification">
                                            <Badge color="secondary">
                                                <NotificationsIcon className="nodification-count" onClick={this.handleNotificationOpen}/>                                                                                             
                                            </Badge>
                                        </Grid>
                                        {this.state.notification ?(
                                            <Grid>
                                                <Notifications></Notifications>
                                            </Grid>
                                        ):null}  
                                        <Grid item className="user-layout">
                                            <Grid container  direction="column" justify="flex-end" alignItems="flex-end" className="user-container" >
                                                <Typography variant="subtitle1" className="login-username">Welcome {this.props.loginData.firstname} {this.props.loginData.lastname} </Typography>
                                                {/* <Typography variant="caption" className="login-userrole">{userRole}</Typography> */}
                                            </Grid>
                                        </Grid>
                                        <Grid item className="user-profile-icon">
                                            <Icon > <img alt="Onboarding" src={userIcon}/> </Icon>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Toolbar>
                </AppBar>
            </Grid>
        )
    }
    }

const mapStateToProps = (state:any) => ({
        loginData:state.loginData
});

export default connect(mapStateToProps)(HeaderComponent);
             