// Redirect dom
import React from "react";
import { Redirect } from "react-router-dom";

// Layout Types
import MainNavBar from './layout/LeftNavLayout'
import LoginLayout from './layout/LoginLayout'
// Route Views
import Login from './views/LoginComponent/Login'
import Dashboard from './views/Dashboard/Dashboard'
import UiUxStyleGuide from './views/uiuxstyleguide/uiUxStyleGuide'
import Authentication from './views/authentication/authentication'
import PushNotification from './views/pushnotification/pushNotification'
import CaptchaImplementation from './views/captchaimplementation/captchaImplementation'
import LoggingErrorHandling from './views/logging&errorhandling/loggingErrorHandling'
import SCAIntegration from './views/scaintegration/scaIntegration'
import REODesigner from './views/reodesigner/reoDesigner'
import LearnMore from './views/learnmore/learnMore'
import DataScheduler from './views/datalist/dataScheduler'


export default [
  {
    path: "/",
    exact: true,
    layout: LoginLayout,
    component: () => <Redirect to="/login" />
  },
  {
    path: "/login",
    layout: LoginLayout,
    component: Login
  },
  {
    path: "/home",
    exact: true,
    layout: MainNavBar,
    component: Dashboard
  },  
  {
    path: "/uiuxstyleguide",
    exact: true,
    layout: MainNavBar,
    component: UiUxStyleGuide
  },
  {
    path: "/authentication",
    exact: true,
    layout: MainNavBar,
    component: Authentication
  },
  {
    path: "/pushnotification",
    exact: true,
    layout: MainNavBar,
    component: PushNotification
  },
  {
    path: "/captchaimplementation",
    exact: true,
    layout: MainNavBar,
    component: CaptchaImplementation
  },
  {
    path: "/loggingerrorhandling",
    exact: true,
    layout: MainNavBar,
    component: LoggingErrorHandling
  },
  {
    path: "/scaintegration",
    exact: true,
    layout: MainNavBar,
    component: SCAIntegration
  },
  {
    path: "/reodesigner",
    exact: true,
    layout: MainNavBar,
    component: REODesigner
  },
  {
    path: "/learnmore",
    layout: LoginLayout,
    component: LearnMore
  },
  {
    path: "/datatable",
    layout: MainNavBar,
    component: DataScheduler
  }
];
