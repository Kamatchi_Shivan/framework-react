import React from "react";
import PropTypes from "prop-types";
import { Grid, Box } from '@material-ui/core';

/**Import of Drawer component and Header component  */
import DrawerComponent from '../components/layout/MainSidebar/DrawerComponent'
import HeaderComponent from '../components/layout/Header/HeaderComponent'

const MainNavbar = ({children}:{children:any}) => {
   
    return (
         <Grid container direction="row" className="page-wrapper">
{/**Render Drawer component and Header component */}
                <Grid item className="page-slidemenu">
                  <Box boxShadow={3}>
                    <DrawerComponent/>
                  </Box>
                </Grid>
                <Grid item className="page-header">
                    <HeaderComponent/>
                </Grid>
                {/**Render child component from router */}
                <Grid container className="page-container">
                    {children}
                </Grid>
            </Grid>
       );
  };

MainNavbar.propTypes = {
  /**
   * The layout type where the MainNavbar is used.
   */
  layout: PropTypes.string,
  /**
   * Whether the main navbar is sticky to the top, or not.
   */
  stickyTop: PropTypes.bool
};

MainNavbar.defaultProps = {
  stickyTop: true
};

export default MainNavbar;
