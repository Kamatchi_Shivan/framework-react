import React from "react";
import PropTypes from "prop-types";
import { Grid } from '@material-ui/core';


const LoginLayout = ({children}:{children:any}) => {
    return (
        <Grid container className="pg-login"  direction="row"  justify="flex-start"  alignItems="flex-start"  >

                   {children}
         </Grid>
      );
    };

LoginLayout.propTypes = {
  /**
   * Whether to display the navbar, or not.
   */
  noNavbar: PropTypes.bool,
  /**
   * Whether to display the footer, or not.
   */
  noFooter: PropTypes.bool
};

LoginLayout.defaultProps = {
  
};

export default LoginLayout;
