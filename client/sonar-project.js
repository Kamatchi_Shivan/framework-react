const sonarqubeScanner = require("sonarqube-scanner");

sonarqubeScanner(
    {
      serverUrl: "http://localhost:9000",
      token: "bdf54d23b8101bbef17fc24bae303bb5bf481d5e",
      options: {
        "sonar.sources": "./src",
        //"sonar.test.inclusions"="**/*.test.js",
        "sonar.exclusions": "**/__tests__/**",
        "sonar.tests": "C:/Users/Azhagu/Projects/expleoframework/client/src/App.test.tsx",
        "sonar.test.inclusions": "./src/__tests__/**/*.test.tsx,./src/__tests__/**/*.test.ts,./src/App.test.tsx,C:/Users/Azhagu/Projects/expleoframework/client/src/App.test.tsx,./src/App.test.tsx",
        "sonar.typescript.lcov.reportPaths": "coverage/lcov.info",
        "sonar.testExecutionReportPaths": "reports/test-report.xml",
      },
    },
    () => {},
  );